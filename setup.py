from setuptools import setup
from setuptools import find_packages


setup(name='rfcnn',
      version='1.0',
      description='Recurrent Fully Convolutional Neural Network',
      author='Sepehr Valipour, Mennatullah Siam',
      author_email='valipour@ulaberta.ca, mennatul@ualberta.ca',
      url='https://gitlab.com/sepehr.valipour/RFCNN.git',
      license='MIT',
      install_requires=['theano'],
      packages=find_packages())
