 
# Recurrent Fully Convolutional Neural Network                                                                                                                                                                                               
                                                                                                                                                                                                                                             
This repository includes the source code for experiments delineated in our RFCNN papers. For more details, please refer to:
                                                                                                                 
<https://arxiv.org/abs/1606.00487>                                                                                                                                                                                                             
<https://arxiv.org/abs/1611.05435>                                                                                                                                                                                                             

## Requirements
* python2.7
* Theano
* numpy, scipy
* h5py

                                                                                                                                                                                                                                             
## Data                                                                                                                                                                                                                                      
The experiments are conducted on 5 publicly available datasets. ChangeDetection, SegTrackV2, Davis, Synthia, CamVid. We converted the raw images into h5 files and resized them for the ease of use. 
You can download the data from here(models will be added later as well):
[Data Files](http://199.116.235.173/rfcnn_data/)

## Run
See the files in the examples. Modify the path inside the files to where you have downloaded the data and run the code. 
