import six.moves.cPickle as pickle
import h5py
import gzip
import os
import sys
import timeit
from theano.tensor.nnet import conv
import numpy
import pdb
import theano
import theano.tensor as T
import scipy
from theano import config
from PIL import Image
import matplotlib.pyplot as plt
from theano import config

####################### Functions for debugging ##############################
def inspect_outputs(i,node,f):
    print(i, 'Node name ',  node, 'Shapes of the output ', f.outputs[0][0].shape)

def inspect_inputs(i, node, f):
     print(i, node, "Shape of input :", f.inputs[0][0].shape)

def load_all_datasets(dataset_names, directory, db_type = 'h5py', load='all', test= True, percent= 1.0):
    # load flag = 'all': for all data, 'data': for data only, 'labels': for labels only
    # test flag= True : to load test data, False: not

    # Load data from npy files
    if db_type == 'npy':
        for i in range(0,len(dataset_names)):
            datasets= numpy.load(directory+dataset_names[i])

            # Check whether to load data only, labels only, or all data for training and validation
            temp_set_x= None
            temp_set_y= None
            if load== 'data':
                temp_set_x, _ = datasets[0]
                temp_set_x= temp_set_x[:percent*temp_set_x.shape[0], :, :]
            elif load=='labels':
                _, temp_set_y = datasets[0]
                temp_set_y= temp_set_y[:percent*temp_set_y.shape[0], :]
            else:
                temp_set_x, temp_set_y = datasets[0]
                temp_set_x= temp_set_x[:percent*temp_set_x.shape[0], :, :]
                temp_set_y= temp_set_y[:percent*temp_set_y.shape[0], :]

            if i==0:
                train_set_x= temp_set_x
                train_set_y=temp_set_y
            else:
                if train_set_x is not None:
                    train_set_x= numpy.concatenate((temp_set_x, train_set_x), axis=0)
                if train_set_y is not None:
                    train_set_y= numpy.concatenate((temp_set_y, train_set_y), axis=0)

            temp_set_x= None
            temp_set_y= None
            if load== 'data':
                temp_set_x, _ = datasets[1]
                temp_set_x= temp_set_x[:percent*temp_set_x.shape[0], :, :]
            elif load=='labels':
                _, temp_set_y = datasets[1]
                temp_set_y= temp_set_y[:percent*temp_set_y.shape[0], :]
            else:
                temp_set_x, temp_set_y = datasets[1]
                temp_set_x= temp_set_x[:percent*temp_set_x.shape[0], :, :]
                temp_set_y= temp_set_y[:percent*temp_set_x.shape[0], :]

            if i==0:
                valid_set_x= temp_set_x
                valid_set_y=temp_set_y
            else:
                if valid_set_x is not None:
                    valid_set_x= numpy.concatenate((temp_set_x, valid_set_x), axis=0)
                if valid_set_y is not None:
                    valid_set_y= numpy.concatenate((temp_set_y , valid_set_y), axis=0)

            # Check whether to load test data or not
            if test:
                temp_set_x, temp_set_y = datasets[2]
                if i==0:
                    test_set_x= temp_set_x
                    test_set_y=temp_set_y
                else:
                    test_set_x= numpy.concatenate((temp_set_x, test_set_x), axis=0)
                    test_set_y= numpy.concatenate((temp_set_y, test_set_y), axis=0)
            else:
                test_set_x=None
                test_set_y= None

    elif db_type == 'h5py':
        for i in range(0,len(dataset_names)):

            hf = h5py.File(directory+dataset_names[i], 'r')
            temp_set_x= None
            temp_set_y= None
            if load== 'all' or load== 'data':
                temp_set_x = numpy.array(hf.get('images_train'))
                temp_set_x= temp_set_x[:percent*temp_set_x.shape[0], :, :]
            if load=='all' or load=='labels':
                temp_set_y = numpy.array(hf.get('labels_train'))
                temp_set_y= temp_set_y[:percent*temp_set_y.shape[0], :]

            if i==0:
                train_set_x= temp_set_x
                train_set_y=temp_set_y
            else:
                if train_set_x is not None:
                    train_set_x= numpy.concatenate((temp_set_x, train_set_x), axis=0)
                if train_set_y is not None:
                    train_set_y= numpy.concatenate((temp_set_y, train_set_y), axis=0)

            hf = h5py.File(directory+dataset_names[i], 'r')
            temp_set_x= None
            temp_set_y= None
            if load=='all' or load=='data':
                temp_set_x = numpy.array(hf.get('images_valid'))
                temp_set_x= temp_set_x[:percent*temp_set_x.shape[0], :, :]
            if load=='all' or load=='labels':
                temp_set_y = numpy.array(hf.get('labels_valid'))
                temp_set_y= temp_set_y[:percent*temp_set_y.shape[0], :]

            if i==0:
                valid_set_x= temp_set_x
                valid_set_y=temp_set_y
            else:
                if valid_set_x is not None:
                    valid_set_x= numpy.concatenate((temp_set_x, valid_set_x), axis=0)
                if valid_set_y is not None:
                    valid_set_y= numpy.concatenate((temp_set_y , valid_set_y), axis=0)

            if test:
                hf = h5py.File(directory+dataset_names[i], 'r')
                temp_set_x = numpy.array(hf.get('images_test'))
                temp_set_y = numpy.array(hf.get('labels_test'))

                if i==0:
                    test_set_x= temp_set_x
                    test_set_y=temp_set_y
                else:
                    test_set_x= numpy.concatenate((temp_set_x, test_set_x), axis=0)
                    test_set_y= numpy.concatenate((temp_set_y, test_set_y), axis=0)
            else:
                test_set_x= None
                test_set_y= None

            hf.close()

    return train_set_x, train_set_y, valid_set_x, valid_set_y, test_set_x, test_set_y


def gen_ped_data(file, path):
    imgList= file.readlines()
    imgList = [x.strip('\n') for x in imgList]
    image= numpy.array(Image.open(path+'input/'+imgList[0]))
    images= numpy.zeros((len(imgList), image.shape[0]*image.shape[1]))
#    print('Original Shape ', image.shape)
    for i in range(0,len(imgList)):
        image= numpy.array(Image.open(path+'input/'+imgList[i]).convert('L'))
        images[i, :]= image.flatten()/255.0

    labels= numpy.zeros((len(imgList), image.shape[0]*image.shape[1]))
    for i in range(0,len(imgList)):
        fname= imgList[i].split('.')[0]
        fname= 'gt'+fname[2:]+'.png'
        image= numpy.array(Image.open(path+'groundtruth/'+fname))>=100
        labels[i, :]= image.flatten()
    return images, labels

def generate_pedestrian_dataset(path):
    file = open(path+'training.txt')
    train_set_x, train_set_y= gen_ped_data(file, path)
    file = open(path+'validation.txt')
    test_set_x, test_set_y= gen_ped_data(file, path)
    numpy.save('../../pedestrians', ((train_set_x, train_set_y), (test_set_x, test_set_y)))

def sliding_window(set_x, set_y, nframes):
    if set_x.ndim == 3:
        temp_set_x= numpy.zeros((set_x.shape[0]-nframes, nframes, set_x.shape[1], set_x.shape[2]), dtype= set_x.dtype)
    else:
        temp_set_x= numpy.zeros((set_x.shape[0]-nframes, nframes, set_x.shape[1]), dtype= set_x.dtype)
    temp_set_y= numpy.zeros((set_y.shape[0]-nframes, nframes, set_y.shape[1]), dtype= set_y.dtype)
    for j in range(0, set_x.shape[0]-nframes):
        for i in range(0, nframes):
            temp_set_x[j,i,:]= set_x[j+i, :]
            temp_set_y[j,i,:]= set_y[j+i, :]
    return temp_set_x, temp_set_y

def zero_weight(ndim):
    return numpy.zeros((ndim, ndim),dtype= config.floatX)

def identity_weight(ndim):
    return numpy.identity(ndim, dtype= config.floatX)

def identity_ortho_weight(ndim):
    ws= numpy.identity(ndim)
    return numpy_floatX(ws + ortho_weight(ndim)*0.05)

def ortho_weight(ndim):
    W = numpy.random.randn(ndim, ndim)
    u, s, v = numpy.linalg.svd(W)
    return u.astype(config.floatX)

def numpy_floatX(data):
    return numpy.asarray(data, dtype=config.floatX)
