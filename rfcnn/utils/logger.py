import time

class Logger(object):
    def __init__(self, log_file, write_mode='w'):
        self.f = open(log_file, write_mode)

    def on_train_begin(self, epoch, logs={}):
        """TODO: Docstring for on_epoch_begin.
        :arg1: TODO
        :returns: TODO
        """
        print("on_train_begin")
        self.start_time = time.time()
        self.f.write("Starting to train at %s\n"%(time.asctime()))
        #self.f.write("Number of epochs are: \n%s "%logs['n_epochs'])
        #self.f.write("batch size is: %s \n"%logs['batch_size'])
        #self.f.write("logs are to be written to %s\n"%(self.log_file))
        #self.f.write("trained model will be saved to %s\n"%(logs['save_path']))
        self.f.write("=======================================================\n\n")

    def on_train_end(self, logs={}):
        print("on_train_end")
        self.f.write("training finished at %s after %s minutes"%(time.asctime(), (time.time()-self.start_time)/60))
        #self.f.write("logs are to be written to %s"%(self.log_file))
        #self.f.write("trained model will be saved to %s"%(logs['save_path']))

    def on_batch_end(self, logs={}):
        """TODO: Docstring for on_batch_end.
        :returns: TODO

        """
        print("batch_end")
        #self.f.write("cost at (%s)th batch of epoch %s is %s\n",
        #        %("iter", logs['epoch'], logs['cost']))

    def on_valid(self, logs={}):
        print("on_valid")

    def on_test(self, logs={}):
        print("on_test")


























