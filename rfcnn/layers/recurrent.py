import numpy
import theano
import theano.tensor as T
from theano.tensor.nnet import conv
from theano import config

from ..utils.generic import numpy_floatX, identity_ortho_weight


class ConvGRU(object):

    """Docstring for convolution. """

    def __init__(self, name, internal_net, filter_size, stride, num_in, num_out, pad=0, lr_adj=1.0, temporal_output='last'):
        """TODO: to be defined1. """
        self.rng = numpy.random.RandomState(23455)
        self.name = name
        self.filter_size = filter_size
        self.stride = stride
        self.num_in = num_in
        self.num_out = num_out
        self.pad = pad
        self.lr_adj = numpy_floatX(lr_adj)
        self.params = {}
        self._set_weights(name, filter_size, num_in, num_out)
        self.temporal_output = temporal_output
        self.net = internal_net
        self.shape = []

    def _set_weights(self, name, fsize, nin, nout):
        """TODO: Docstring for set_weights.
        :returns: TODO
        """
        w_shp = (nout, nin, fsize, fsize)
        w_bound = numpy.sqrt(nin*fsize*fsize)
        self.params[name+'_Wr'] = theano.shared( numpy.asarray(
                    self.rng.uniform(low=-1.0 / w_bound,high=1.0 / w_bound,size=w_shp),
                    dtype=theano.config.floatX), name = name + '_Wr')
        self.params[name+'_Ur'] = theano.shared( numpy.asarray(
                    self.rng.uniform(low=-1.0 / w_bound,high=1.0 / w_bound,size=w_shp),
                    dtype=theano.config.floatX), name = name + '_Ur')
        self.params[name+'_Wz'] = theano.shared( numpy.asarray(
                    self.rng.uniform(low=-1.0 / w_bound,high=1.0 / w_bound,size=w_shp),
                    dtype=theano.config.floatX), name = name + '_Wz')
        self.params[name+'_Uz'] = theano.shared( numpy.asarray(
                    self.rng.uniform(low=-1.0 / w_bound,high=1.0 / w_bound,size=w_shp),
                    dtype=theano.config.floatX), name = name + '_Uz')
        self.params[name+'_W'] = theano.shared( numpy.asarray(
                    self.rng.uniform(low=-1.0 / w_bound,high=1.0 / w_bound,size=w_shp),
                    dtype=theano.config.floatX), name = name + '_W')
        self.params[name+'_U'] = theano.shared( numpy.asarray(
                    self.rng.uniform(low=-1.0 / w_bound,high=1.0 / w_bound,size=w_shp),
                    dtype=theano.config.floatX), name = name + '_U')

        b_shp = (nout,)
        self.params[name+'_br'] = theano.shared(numpy.asarray(
                self.rng.uniform(low=-.5, high=.5, size=b_shp),
                dtype=theano.config.floatX), name =name + '_br')
        self.params[name+'_bz'] = theano.shared(numpy.asarray(
                self.rng.uniform(low=-.5, high=.5, size=b_shp),
                dtype=theano.config.floatX), name =name + '_bz')
        self.params[name+'_b'] = theano.shared(numpy.asarray(
                self.rng.uniform(low=-.5, high=.5, size=b_shp),
                dtype=theano.config.floatX), name =name + '_b')

    def __call__(self, layer_input):
        """TODO: Docstring for __call__.
        :returns: TODO
        """
        nsteps = layer_input.shape[1]
        if  layer_input.ndim == 4 or layer_input.ndim == 3:
            n_samples = layer_input.shape[0]
        else:
            raise ValueError("Wrong input data shape for conv_gru")

        def _step(frame, h_):

            self.internal_model = self.net.compile(layer_input=frame)

            pad_x = T.zeros((self.internal_model.shape[0], self.internal_model.shape[1],
                    self.internal_model.shape[2]+2*self.pad, self.internal_model.shape[3]+2*self.pad))
            pad_x = T.set_subtensor(pad_x[:,:,self.pad:self.internal_model.shape[2]+self.pad,
                    self.pad:self.internal_model.shape[3]+self.pad], self.internal_model)

            pad_h = T.zeros((h_.shape[0], h_.shape[1],
                            (h_.shape[2]-1)*self.stride+self.filter_size,
                            (h_.shape[3]-1)*self.stride+self.filter_size))
            diff_pad = (pad_h.shape[2] - h_.shape[2])//2
            pad_h = T.set_subtensor(pad_h[:,:,diff_pad:h_.shape[2]+diff_pad, diff_pad:h_.shape[3]+diff_pad], h_)


            r = T.nnet.sigmoid(conv.conv2d(pad_x, self.params[self.name+'_Wr'], subsample=(self.stride,self.stride)) + \
                    conv.conv2d(pad_h, self.params[self.name+'_Ur'], subsample=(self.stride,self.stride)) + \
                    self.params[self.name+'_br'].dimshuffle('x', 0, 'x', 'x') )

            z = T.nnet.sigmoid(conv.conv2d(pad_x, self.params[self.name+'_Wz'], subsample=(self.stride,self.stride)) + \
                    conv.conv2d(pad_h, self.params[self.name+'_Uz'], subsample=(self.stride,self.stride)) + \
                    self.params[self.name+'_bz'].dimshuffle('x', 0, 'x', 'x') )

            #pad_r = T.zeros((r.shape[0], r.shape[1], r.shape[2]+2*self.pad, r.shape[3]+2*self.pad))
            #pad_r = T.set_subtensor(pad_r[:,:,self.pad:r.shape[2]+self.pad, self.pad:r.shape[3]+self.pad], r)

            pad_r = T.zeros(pad_x.shape)
            pad_r = T.set_subtensor(pad_r[:,:,diff_pad:r.shape[2]+diff_pad, diff_pad:r.shape[3]+diff_pad], r)

            X = conv.conv2d(pad_x, self.params[self.name+'_W'], subsample=(self.stride,self.stride)) + \
                    conv.conv2d(pad_h*pad_r, self.params[self.name+'_U'], subsample=(self.stride,self.stride)) + \
                    self.params[self.name+'_b'].dimshuffle('x', 0, 'x', 'x')

            h_hat = T.nnet.relu(X, alpha = 0.5)
            h = z*h_ + (1-z)*h_hat
            return h

        if layer_input.ndim == 3:
            dim_shuffle = [1,0,2]
        elif layer_input.ndim == 4:
            dim_shuffle = [1,0,2,3]
        rval, updates = theano.scan(_step, \
                sequences=layer_input.dimshuffle(dim_shuffle), \
                outputs_info=[T.unbroadcast( T.alloc(numpy_floatX(1.),
                                                n_samples,
                                                self.shape[0],
                                                self.shape[1],
                                                self.shape[2]), 1)], \
                name='_layers', \
                n_steps=nsteps)

        if self.temporal_output == 'last':
            return rval[-1]
        elif self.temporal_output == 'all':
            return rval

    def get_shape(self, input_shape):
        """
        TODO
        """
        input_shape = self.net.get_shape(input_shape)
        self.shape = [self.num_out,
                (input_shape[1] - self.filter_size + 2*self.pad)/self.stride + 1,
                (input_shape[2] - self.filter_size + 2*self.pad)/self.stride + 1]
        return self.shape

class GRU(object):
    """ TODO """

    def __init__(self, name,internal_net, lr_adj=1.0, temporal_output='last'):
        """TODO """
        self.name = name
        self.lr_adj = numpy_floatX(lr_adj)
        internal_shape= internal_net.get_shape()
        self.num_in = internal_shape[1]*internal_shape[2]
        self.params = {}
        self._set_weights(name, self.num_in)
        self.temporal_output = temporal_output
        self.net = internal_net
        self.shape = []

    def _set_weights(self, name, num_in):
        """TODO  """
        W = numpy.concatenate([identity_ortho_weight(num_in), identity_ortho_weight(num_in), identity_ortho_weight(num_in)], axis=1)
        U = numpy.concatenate([identity_ortho_weight(num_in), identity_ortho_weight(num_in), identity_ortho_weight(num_in)], axis=1)
        b = numpy.zeros((3 * num_in,))
        b = b.astype(config.floatX)

        self.params[self.name + '_W'] = theano.shared(value=W, name=self.name+'_W')
        self.params[self.name + '_U'] = theano.shared(value=U, name=self.name+'_U')
        self.params[self.name + '_b'] = theano.shared(value=b, name=self.name+'_b')

    def get_shape(self, input_shape):
        """TODO  """
        input_shape = self.net.get_shape(input_shape)
        #Since it can only work on last layers so num_out=1
        self.shape = [1,input_shape[1], input_shape[2]]
        return self.shape


    def __call__(self, layer_input):
        """TODO """
        nsteps = layer_input.shape[1]
        if layer_input.ndim == 3 or layer_input.ndim == 4:
            n_samples = layer_input.shape[0]
        else:
            n_samples = 1

        def _slice(_x, n, dim, width = 1):
            if _x.ndim == 3:
                return _x[:, :, n * dim:(n + 1) * dim]
            return _x[:, n * dim:(n + width) * dim]

        def _step(frame, h_):
            self.internal_model = self.net.compile(layer_input=frame)
            self.prev_layer_shape= self.net.get_shape(layer_input.shape)

            if self.internal_model.ndim==4:
                self.internal_model= self.internal_model.reshape((self.internal_model.shape[0], self.internal_model.shape[2]*self.internal_model.shape[3]))

            preact = T.dot(h_, _slice(self.params[self.name+'_U'], 0, self.num_in, 2)) + \
                    T.dot(self.internal_model, _slice(self.params[self.name+'_W'], 0, self.num_in, 2)) + \
                    self.params[self.name+'_b'][0:2*self.num_in]

            z = T.nnet.sigmoid(_slice(preact, 0, self.num_in))
            r = T.nnet.sigmoid(_slice(preact, 1, self.num_in))

            h_hat= T.dot(self.internal_model, _slice(self.params[self.name+'_W'], 2, self.num_in)) + \
                    T.dot(r*h_, _slice(self.params[self.name+'_U'], 2, self.num_in)) + \
                    self.params[self.name+'_b'][2*self.num_in:]

            h = z*h_ + (1-z)*h_hat

            return h

        if layer_input.ndim == 3:
            dim_shuffle = [1,0,2]
        elif layer_input.ndim == 4:
            dim_shuffle = [1,0,2,3]

        rval, updates = theano.scan(_step, sequences=layer_input.dimshuffle(dim_shuffle), \
                outputs_info=[T.alloc(numpy_floatX(5.), n_samples, self.num_in)], \
                name='_layers', n_steps=nsteps)
        rval= rval.reshape((rval.shape[0], n_samples,1, self.prev_layer_shape[1], self.prev_layer_shape[2]))

        if self.temporal_output == 'last':
            return rval[-1]
        elif self.temporal_output == 'all':
            return rval


