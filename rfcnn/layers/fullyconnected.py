import numpy
import theano
import theano.tensor as T
from theano import config


from ..utils.generic import numpy_floatX

class FullyConnected(object):

    """Docstring for convolution. """

    def __init__(self, name, num_in, num_out, lr_adj=1.0):
        """TODO: to be defined1. """
        self.rng = numpy.random.RandomState(23455)
        self.name = name
        self.num_in = num_in
        self.num_out = num_out
        self.lr_adj = numpy_floatX(lr_adj)
        self.params = {}
        self._set_weights(name, num_in, num_out)



    def _set_weights(self, name, num_in, num_out):
        """TODO: Docstring for set_weights.
        :returns: TODO
        """
        W_values = numpy.asarray(
            self.rng.uniform(
                low=-numpy.sqrt(6. / (num_in + num_out)),
                high=numpy.sqrt(6. / (num_in + num_out)),
                size=(n_in, n_out)),dtype=theano.config.floatX)
        b_values = numpy.zeros((num_out,), dtype=theano.config.floatX)
        self.params[name+'_W'] = theano.shared(value=W_values, name =name + '_W', borrow=True)
        self.params[name+'_b'] = theano.shared(value=b_values, name= name + '_b', borrow=True)

    def __call__(self, layer_input):
        """TODO: Docstring for __call__.
        :returns: TODO
        """
        layer_input = layer_input.flatten(2)
        return T.dot(layer_input, self.params[name+'_W']) + self.params[name+'_b']

    def get_shape(self, input_shape):
        """
        TODO
        """
        return [self.num_out]





