import numpy
import theano
import theano.tensor as T
from theano import config
import pdb
from rfcnn.layers import Conv
from ..utils.generic import numpy_floatX

class Concat(object):
    """Concatenates multiple inputs """
    def __init__(self, name, bottom):
        """TODO: to be defined1. """
        self.name = name
        self.bottom = bottom

    def __call__(self, comp_graphs):
        """TODO: Docstring for __call__.
        :returns: TODO
        """
        out = None
        for k,v in comp_graphs.items():
            if k in self.bottom:
                if out is None:
                    out = v
                else:
                    out = T.concatenate((out, v), axis=1)
        return out

    def get_shape(self, shapes):
        """
        TODO
        """
        out_shape = None
        for k, v in shapes.items():
            if k in self.bottom:
                if out_shape is None:
                    out_shape = v[:]
                else:
                    out_shape[0] = out_shape[0] + v[0]
        #print("out_shape is {}".format(out_shape))
        return out_shape

class Skip(object):
    """
    Implementation of skip architecture for finer strides in FCN
    This layer takes n bottom layers along with n upsampling ratio
    then it upsample corresponging imputs and concatenate the results.
    """
    def __init__(self, name, bottom, upsample_ratios):
        """TODO: to be defined1. """
        self.name = name
        self.bottom = bottom
        self.ratios = upsample_ratios

    def __call__(self, comp_graphs):
        """TODO: Docstring for __call__.
        :returns: TODO
        """
        out = None
        for k,v in comp_graphs.items():
            if k in self.bottom:
                ratio = self.ratios[self.bottom.index(k)]
                if out is None:
                    if ratio == 1:
                        out = v[:,:,0:self.out_shape[1],0:self.out_shape[2]]
                    else:
                        out = T.nnet.abstract_conv.bilinear_upsampling(v, ratio)[:,:,0:self.out_shape[1],0:self.out_shape[2]]
                else:
                    if ratio == 1:
                        out = T.concatenate((out, v[:,:,0:self.out_shape[1],0:self.out_shape[2]]), axis=1)
                    else:
                        out = T.concatenate((out,
                                            T.nnet.abstract_conv.bilinear_upsampling(v, ratio)[:,:,
                                                                                0:self.out_shape[1],
                                                                                0:self.out_shape[2]]),
                                                                                                axis=1)
        return out

    def get_shape(self, shapes):
        """
        TODO
        """
        out_shape = None
        min_spatial=[numpy.inf, numpy.inf]
        for k, v in shapes.items():
            if k in self.bottom:
                print(k)
                ratio = self.ratios[self.bottom.index(k)]
                temp_shape = [ratio*s for s in v[1:]]
                if temp_shape[0]<min_spatial[0]:
                    min_spatial[0] = temp_shape[0]
                if temp_shape[1]<min_spatial[1]:
                    min_spatial[1] = temp_shape[1]

                if out_shape is None:
                    out_shape = v[:]
                else:
                    out_shape[0] = out_shape[0] + v[0]
                print(out_shape)
        out_shape[1:] = min_spatial

        print("out_shape is {}".format(out_shape))
        self.out_shape = out_shape
        return out_shape


class SkipHmaps(object):
    """
    Implementation of skip architecture for finer strides in FCN
    This layer takes n bottom layers along with n upsampling ratio
    then it upsample corresponging imputs and concatenate the results.
    """
    def __init__(self, name, bottom, upsample_ratios, channels, nclasses, lr_adj= 1.0):
        """TODO: to be defined1. """
        self.name = name
        self.bottom = bottom
        self.ratios = upsample_ratios
        self.nclasses= nclasses
        self.lr_adj = numpy_floatX(lr_adj)
        self.c_htmaps= {}
        self.params= {}
        i=0
        for b in self.bottom:
            self.c_htmaps[b]= Conv('skip_'+b,3,1,channels[i], self.nclasses, padding=1, lr_adj=self.lr_adj)
            i+=1
            self.params = self.merge_two_dicts(self.c_htmaps[b].params, self.params)

    def merge_two_dicts(self, x, y):
        '''Given two dicts, merge them into a new dict as a shallow copy.'''
        z = x.copy()
        z.update(y)
        return z

    def __call__(self, comp_graphs):
        """TODO: Docstring for __call__.
        :returns: TODO
        """

        out = None
        for k,v in comp_graphs.items():
            if k in self.bottom:
                ratio = self.ratios[self.bottom.index(k)]

                #Pass it through Convolutional layer
                temp_out= self.c_htmaps[k](v)

                if out is None:
                    if ratio == 1:
                        out = temp_out[:,:,0:self.out_shape[1],0:self.out_shape[2]]
                    else:
                        out = T.nnet.abstract_conv.bilinear_upsampling(temp_out, ratio)[:,:,0:self.out_shape[1],0:self.out_shape[2]]
                else:
                    if ratio == 1:
                        out = T.sum((out, temp_out[:,:,0:self.out_shape[1],0:self.out_shape[2]]), axis=1)
                    else:
                        out = T.sum((out,
                                            T.nnet.abstract_conv.bilinear_upsampling(temp_out, ratio)[:,:,
                                                                                0:self.out_shape[1],
                                                                                0:self.out_shape[2]]),
                                                                                                axis=1)
        return out

    def get_shape(self, shapes):
        """
        TODO
        """
        out_shape = None
        min_spatial=[numpy.inf, numpy.inf]
        for k, v in shapes.items():
            if k in self.bottom:
                print(k)
                ratio = self.ratios[self.bottom.index(k)]
                temp_shape = [ratio*s for s in v[1:]]
                if temp_shape[0]<min_spatial[0]:
                    min_spatial[0] = temp_shape[0]
                if temp_shape[1]<min_spatial[1]:
                    min_spatial[1] = temp_shape[1]

                if out_shape is None:
                    out_shape = v[:]
                else:
                    out_shape[0] = out_shape[0] + v[0]
                print(out_shape)
        out_shape[1:] = min_spatial
        out_shape[0]= self.nclasses
        print("out_shape is {}".format(out_shape))
        self.out_shape = out_shape
        return out_shape

